# NachOS

* Lecture : NCU 1062 Operating System
* Language : `c++`
* Environment : Ubuntu 14.04 32bit
* Note : This NachOS is ***modified*** by TA for assigning homework to us.
	* Project_1 : **Multiprogramming**
        * `code/userprog/addrspace.cc`
        * `code/userprog/addrspace.h`
	* Project_2 : **Sleep** syscall
        * Part_1 : Sleep syscall handle & testing program
            * `code/test/Makefile`
            * `code/test/sleep.c`
            * `code/test/start.s`
            * `code/userprog/exception.cc`
            * `code/userprog/syscall.h`
        * Part_2 : Sleep syscall implement
            * `code/threads/alarm.cc`
            * `code/threads/alarm.h`