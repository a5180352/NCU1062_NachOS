// alarm.cc
//     Routines to use a hardware timer device to provide a
//     software alarm clock.  For now, we just provide time-slicing.
//
//     Not completely implemented.
//
// Copyright (c) 1992-1996 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "alarm.h"
#include "main.h"

//----------------------------------------------------------------------
// Alarm::Alarm
//     Initialize a software alarm clock.  Start up a timer device
//
//     "doRandom" -- if true, arrange for the hardware interrupts to
//     occur at random, instead of fixed, intervals.
//----------------------------------------------------------------------

Alarm::Alarm(bool doRandom) {
    timer = new Timer(doRandom, this);
}

// [Proj_2] Implement Alarm::WaitUntil
void Alarm::WaitUntil(int x) {
    IntStatus oldLevel = kernel->interrupt->SetLevel(IntOff);
    Thread* t = kernel->currentThread;
    cout << "Thread: " << t << " sleep " << x << " ms." << endl;

    ASSERT(kernel->interrupt->getLevel() == IntOff);
    sleepedThread.push_back(make_pair(t, x));
    t->Sleep(false);

    kernel->interrupt->SetLevel(oldLevel);
}
// ===================================

//----------------------------------------------------------------------
// Alarm::CallBack
//     Software interrupt handler for the timer device. The timer device is
//     set up to interrupt the CPU periodically (once every TimerTicks).
//     This routine is called each time there is a timer interrupt,
//     with interrupts disabled.
//
//     Note that instead of calling Yield() directly (which would
//     suspend the interrupt handler, not the interrupted thread
//     which is what we wanted to context switch), we set a flag
//     so that once the interrupt handler is done, it will appear as
//     if the interrupted thread called Yield at the point it is
//     was interrupted.
//
//     For now, just provide time-slicing.  Only need to time slice
//     if we're currently running something (in other words, not idle).
//     Also, to keep from looping forever, we check if there's
//     nothing on the ready list, and there are no other pending
//     interrupts.  In this case, we can safely halt.
//----------------------------------------------------------------------

void Alarm::CallBack() {
    Interrupt *interrupt = kernel->interrupt;
    MachineStatus status = interrupt->getStatus();

    // [Proj_2] Check which thread should wake up
    bool anyone_waken = false;
    for (vector<pair<Thread*, int> >::iterator it = sleepedThread.begin(); it != sleepedThread.end(); ) {
        if (it->second == 0) {
            anyone_waken = true;
            cout << "Thread: " << it->first << " wake up!" << endl;
            kernel->scheduler->ReadyToRun(it->first);
            it = sleepedThread.erase(it);
        } else {
            it->second--;
            it++;
        }
    }
    // ==========================================

    if (status == IdleMode &&   // is it time to quit?
            // [Proj_2] If nothing should be executed, stop timer
            !anyone_waken && sleepedThread.empty()) {
            // ==================================================
        if (!interrupt->AnyFutureInterrupts()) {
            timer->Disable();   // turn off the timer
        }
    } else {                    // there's someone to preempt
        interrupt->YieldOnReturn();
    }
}

