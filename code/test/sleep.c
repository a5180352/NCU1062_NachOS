// [Proj_2] For testing <Sleep> system call
#include "syscall.h"

int main() {
    int i;
    for (i = 0; i < 5; i++) {
        Sleep(5000000);
        PrintInt(50);
    }
}
// ========================================
